//
//  PopOverViewController.swift
//  DateDiffCounter
//
//  Created by salar on 10/22/20.
//  Copyright © 2020 none. All rights reserved.
//

import Cocoa
import AppKit

class PopOverViewController: NSViewController, NSDatePickerCellDelegate {
    
    @IBOutlet weak var picker:NSDatePicker?
    private var lastDate:Date!
    private var timer:Timer!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        
        updateFromUserDefaults()
        timer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: true, block: {(tiemr) in
            self.updateFromUserDefaults()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        picker?.delegate = self
        picker?.target = self
        picker?.dateValue = lastDate
        updateTitle(lastDate)
    }
    
    func updateFromUserDefaults() {
        lastDate = DateHelper.getDates().1
        updateTitle(lastDate)
    }
    
    
    func datePickerCell(_ datePickerCell: NSDatePickerCell, validateProposedDateValue proposedDateValue: AutoreleasingUnsafeMutablePointer<NSDate>, timeInterval proposedTimeInterval: UnsafeMutablePointer<TimeInterval>?) {
        
        let target = Date(timeIntervalSinceReferenceDate: proposedDateValue.pointee.timeIntervalSinceReferenceDate)
        updateTitle(target)
    }
    
    func updateTitle(_ target:Date)
    {
        lastDate = target
        DateHelper.saveSecondDate(target)
        AppDelegate.instance?.updateTitle(String(describing: DateHelper.remainingDays()) + "👮")
    }
}
