//
//  DaysMenuItemView.swift
//  DateDiffCounter
//
//  Created by salar on 10/23/20.
//  Copyright © 2020 none. All rights reserved.
//

import Cocoa

class DaysMenuItemView: NSView {

    @IBOutlet weak var daysLbl: NSTextField!
    private var lastDate:Date!
    
    static var instance:DaysMenuItemView?
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
        updateFromUserDefaults()
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: {(tiemr) in
            self.updateFromUserDefaults()
        })
        
        DaysMenuItemView.instance = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        updateFromUserDefaults()
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: {(tiemr) in
            self.updateFromUserDefaults()
        })
    }
    
    
    func updateFromUserDefaults(){
        lastDate = DateHelper.getDates().1
        updateTitle(lastDate)
    }
    
    func updateTitle(_ target:Date)
    {
        //picker?.dateValue = target
        lastDate = target
        let cal = NSCalendar.current
        let days = cal.dateComponents([.day], from: NSDate.now, to: target).day ?? 0
        
        let val = String(describing: days)
        daysLbl?.stringValue = val
        AppDelegate.instance?.updateTitle(val + "👮")
        UserDefaults.standard.set(target.timeIntervalSince1970, forKey: "date")
    }
    
}
