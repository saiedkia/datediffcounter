//
//  AppDelegate.swift
//  DateDiffCounter
//
//  Created by salar on 10/22/20.
//  Copyright © 2020 none. All rights reserved.
//

import Cocoa

//https://www.appcoda.com/macos-status-bar-apps/

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    private(set) static var instance:AppDelegate?
    var statusItem: NSStatusItem?
    var popOver: NSPopover?
    
    @IBOutlet weak var menu: NSMenu?
    @IBOutlet weak var firstMenuItem: NSMenuItem?
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        AppDelegate.instance = self
        statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
        statusItem?.button?.title = "0"
        
        if let menu = self.menu {
            statusItem?.menu = menu
            configureMenuFirstItemView()
        } else {
            configurePopOver()
        }
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}



extension AppDelegate : NSMenuDelegate {
    @IBAction func showPreferences(_ sender:Any)
    {
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateController(withIdentifier: "preferencesView") as? ViewController {
            let window = NSWindow(contentViewController: vc)
            window.makeKeyAndOrderFront(nil)
        }
    }
    
    
    func configurePopOver(_ sender:Any? = nil)
    {
        popOver = NSPopover()
        popOver?.behavior = .transient
        
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateController(withIdentifier: "popOverView") as? PopOverViewController {
            popOver?.contentViewController = vc
            statusItem?.button?.target = self
            statusItem?.button?.action = #selector(showPopOver)
        }
    }
    
    
    @objc fileprivate func showPopOver()
    {
        let statusButton = statusItem!.button!
        popOver?.show(relativeTo: statusButton.bounds, of: statusButton, preferredEdge: .minY)
    }
    
    func configureMenuFirstItemView() {
        if let fItem = firstMenuItem {
            var topLevelArray: NSArray?
            Bundle.main.loadNibNamed(NSNib.Name("DaysMenuItemView"), owner: self, topLevelObjects: &topLevelArray)
            
            guard let results = topLevelArray as? [Any],
                let foundedView = results.last(where: {$0 is DaysMenuItemView}),
                let view = foundedView as? NSView else {
                    fatalError("NIB not found")
            }
            
            fItem.view =  view
        }
    }
    
    func updateTitle(_ title:String) {
        AppDelegate.instance?.statusItem?.button?.title = title
    }
    
}

