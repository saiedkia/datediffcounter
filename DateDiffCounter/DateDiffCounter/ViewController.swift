//
//  ViewController.swift
//  DateDiffCounter
//
//  Created by salar on 10/22/20.
//  Copyright © 2020 none. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSDatePickerCellDelegate {

    @IBOutlet weak var okBtn: NSButton!
    @IBOutlet weak var cancelBtn: NSButton!
    
    
    @IBOutlet weak var firstDatePicker: NSDatePicker!
    @IBOutlet weak var secondDatePicker: NSDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let prevSavedDate = UserDefaults.standard.integer(forKey: "date")
        let lastDate = (prevSavedDate > 0) ? Date(timeIntervalSince1970: TimeInterval(prevSavedDate)) : NSDate.now
        secondDatePicker.dateValue = lastDate
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
        view.window?.styleMask.remove(.resizable)
        view.window?.styleMask.remove(.miniaturizable)
        title = "Preferences"
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    @IBAction func okBtnHandler(_ sender: Any) {
        DaysMenuItemView.instance?.updateTitle(secondDatePicker.dateValue)
        cancelBtnHandler(self)
    }
    
    @IBAction func cancelBtnHandler(_ sender: Any) {
        self.view.window?.close()
    }
    
    
    
    func datePickerCell(_ datePickerCell: NSDatePickerCell, validateProposedDateValue proposedDateValue: AutoreleasingUnsafeMutablePointer<NSDate>, timeInterval proposedTimeInterval: UnsafeMutablePointer<TimeInterval>?) {
    }
}

