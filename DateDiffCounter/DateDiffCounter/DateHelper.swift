//
//  DateHelper.swift
//  DateDiffCounter
//
//  Created by salar on 10/25/20.
//  Copyright © 2020 none. All rights reserved.
//

import Foundation



class DateHelper {
    private static let _firstDateKey = "firstDate"
    private static let _secondDateKey = "secondDate"
    
    static func getDates() -> (Date, Date) {
        let firstSavedValue = UserDefaults.standard.integer(forKey: _firstDateKey)
        let firstSavedDate = (firstSavedValue > 0) ? Date(timeIntervalSince1970: TimeInterval(firstSavedValue)) : NSDate.now
        
        let secondSavedValue = UserDefaults.standard.integer(forKey: _firstDateKey)
        let secondSavedDate = (secondSavedValue > 0) ? Date(timeIntervalSince1970: TimeInterval(secondSavedValue)) : NSDate.now
        
        
        return (firstSavedDate, secondSavedDate)
    }
    
    static func saveFirstDate(_ date:Date) {
        UserDefaults.standard.set(date.timeIntervalSince1970, forKey: _firstDateKey)
    }
    
    static func saveSecondDate(_ date:Date) {
        UserDefaults.standard.set(date.timeIntervalSince1970, forKey: _secondDateKey)
    }
    
    static func remainingDays() -> Int {
        
        let dates = getDates()
        
        let cal = NSCalendar.current
        return cal.dateComponents([.day], from: dates.0, to: dates.1).day ?? 0
    }
}
